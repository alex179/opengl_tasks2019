#include <Mesh.hpp>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>


struct Point{
    float x;
    float y;
    float z;

    Point(float x = 0, float y = 0, float z = 0): x(x), y(y), z(z) {}
};

void add_texc(std::vector<glm::vec2>& texcoords, int type, bool up){
    if (type == 0){
        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));
        return;
    }
    if (!up){
        switch (type){
            case 1:
                texcoords.push_back(glm::vec2(0.0, 0.0));
                texcoords.push_back(glm::vec2(0.33, 0.0));
                texcoords.push_back(glm::vec2(0.0, 0.5));
                break;
            case 2:
                texcoords.push_back(glm::vec2(0.34, 0.0));
                texcoords.push_back(glm::vec2(0.66, 0.0));
                texcoords.push_back(glm::vec2(0.34, 0.5));
                break;
            case 3:
                texcoords.push_back(glm::vec2(0.67, 0.0));
                texcoords.push_back(glm::vec2(1.0, 0.0));
                texcoords.push_back(glm::vec2(0.67, 0.5));
                break;
            case 4:
                texcoords.push_back(glm::vec2(0.0, 0.5));
                texcoords.push_back(glm::vec2(0.33, 0.5));
                texcoords.push_back(glm::vec2(0.0, 1.0));
                break;
            case 5:
                texcoords.push_back(glm::vec2(0.34, 0.5));
                texcoords.push_back(glm::vec2(0.66, 0.5));
                texcoords.push_back(glm::vec2(0.34, 1.0));
                break;
            case 6:
                texcoords.push_back(glm::vec2(0.67, 0.5));
                texcoords.push_back(glm::vec2(1.0, 0.5));
                texcoords.push_back(glm::vec2(0.67, 1.0));
                break;
        }
    }else{
        switch (type) {
            case 1:
                texcoords.push_back(glm::vec2(0.33, 0.0));
                texcoords.push_back(glm::vec2(0.0, 0.5));
                texcoords.push_back(glm::vec2(0.33, 0.5));
                break;
            case 2:
                texcoords.push_back(glm::vec2(0.66, 0.0));
                texcoords.push_back(glm::vec2(0.34, 0.5));
                texcoords.push_back(glm::vec2(0.66, 0.5));
                break;
            case 3:
                texcoords.push_back(glm::vec2(1.0, 0.0));
                texcoords.push_back(glm::vec2(0.67, 0.5));
                texcoords.push_back(glm::vec2(1.0, 0.5));
                break;
            case 4:
                texcoords.push_back(glm::vec2(0.33, 0.5));
                texcoords.push_back(glm::vec2(0.0, 1.0));
                texcoords.push_back(glm::vec2(0.33, 1.0));
                break;
            case 5:
                texcoords.push_back(glm::vec2(0.66, 0.5));
                texcoords.push_back(glm::vec2(0.34, 1.0));
                texcoords.push_back(glm::vec2(0.66, 1.0));
                break;
            case 6:
                texcoords.push_back(glm::vec2(1.0, 0.5));
                texcoords.push_back(glm::vec2(0.67, 1.0));
                texcoords.push_back(glm::vec2(1.0, 1.0));
                break;
        }
    }
}


void add_wall_h(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& texcoords, float cell_size, Point p, Point q, bool final, float h, bool t){
    glm::vec3 normal;
    p.x = p.x*cell_size;
    p.y = p.y*cell_size;
    q.x = q.x*cell_size;
    q.y = q.y*cell_size;
    h = h*cell_size;
    if (abs(p.x - q.x) > abs(p.y - q.y)){
        if (t) {
            normal = glm::vec3(0.0, 1.0, 0.0);
        }else{
            normal = glm::vec3(0.0, -1.0, 0.0);
        }
    }
    else{
        if (t){
            normal = glm::vec3(1.0, 0.0, 0.0);
        }else{
            normal = glm::vec3(-1.0, 0.0, 0.0);
        }

    }
    if (final){
        normal = glm::vec3(0.0, 0.0, 0.0);
    }
    vertices.push_back(glm::vec3(p.x, p.y, 0));
    vertices.push_back(glm::vec3(q.x, q.y, 0));
    vertices.push_back(glm::vec3(p.x, p.y, h));

    normals.push_back(normal);
    normals.push_back(normal);
    normals.push_back(normal);

    if (p.x < 1 and p.y < 5){
        add_texc(texcoords, 1, 0);
    } else if (p.x < 1 and p.y >= 5)
    {
        add_texc(texcoords, 2, 0);
    } else if (p.x >= 1 and p.y < 5)
    {
        add_texc(texcoords, 4, 0);
    } else
    {
        add_texc(texcoords, 5, 0);
    }

    vertices.push_back(glm::vec3(q.x, q.y, 0));
    vertices.push_back(glm::vec3(p.x, p.y, h));
    vertices.push_back(glm::vec3(q.x, q.y, h));

    normals.push_back(normal);
    normals.push_back(normal);
    normals.push_back(normal);


    if (p.x < 1 and p.y < 5){
        add_texc(texcoords, 1, 1);
    } else if (p.x < 1 and p.y >= 5)
    {
        add_texc(texcoords, 2, 1);
    } else if (p.x >= 1 and p.y < 5)
    {
        add_texc(texcoords, 4, 1);
    } else
    {
        add_texc(texcoords, 5, 1);
    }

}

void add_wall(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& texcoords, float cell_size, Point p, Point q, bool final = false, float h = 1.0){
    glm::vec3 normal;
    p.x = p.x*cell_size;
    p.y = p.y*cell_size;
    q.x = q.x*cell_size;
    q.y = q.y*cell_size;
    h = h*cell_size;
    if (abs(p.x - q.x) > abs(p.y - q.y)){
        p.y += 0.01;
        q.y += 0.01;
        add_wall_h(vertices, normals, texcoords, cell_size, p, q, final, h, true);
        p.y -= 0.02;
        q.y -= 0.02;
        add_wall_h(vertices, normals, texcoords, cell_size, p, q, final, h, false);
        p.y += 0.01;
        q.y += 0.01;
    }else{
        p.x += 0.01;
        q.x += 0.01;
        add_wall_h(vertices, normals, texcoords, cell_size, p, q, final, h, true);
        p.x -= 0.02;
        q.x -= 0.02;
        add_wall_h(vertices, normals, texcoords, cell_size, p, q, final, h, false);
        p.x += 0.01;
        q.x += 0.01;

    }

}


//void add_wall(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& texcoords, float cell_size, Point p, Point q, bool final = false, float h = 1.0){
//    if (p.x == q.x){
//        p.x += 0.01;
//        q.x += 0.01;
//        t_add_wall(vertices, normals, texcoords, cell_size, p, q, final, h);
//        p.x -= 0.02;
//        q.x -= 0.02;
//        t_add_wall(vertices, normals, texcoords, cell_size, p, q, final, h);
//    }
//    else{
//        p.y += 0.01;
//        q.y += 0.01;
//        t_add_wall(vertices, normals, texcoords, cell_size, p, q, final, h);
//        p.y -= 0.02;
//        q.y -= 0.02;
//        t_add_wall(vertices, normals, texcoords, cell_size, p, q, final, h);
//    }
//}

void add_floor(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& texcoords, int size, float cell_size, float x, float y){
    glm::vec3 normal = glm::vec3(0.0, 0.0, 1.0);

    float X = x * cell_size;
    float Y = y * cell_size;
    float length = size * cell_size;

    for (x = X; x < X + length; x += cell_size){
        for (y = Y; y < Y + length; y += cell_size){
            vertices.push_back(glm::vec3(x, y, 0));
            vertices.push_back(glm::vec3(x, y + cell_size, 0));
            vertices.push_back(glm::vec3(x + cell_size, y, 0));

            normals.push_back(normal);
            normals.push_back(normal);
            normals.push_back(normal);


            add_texc(texcoords, 6, 0);

            vertices.push_back(glm::vec3(x, y + cell_size, 0));
            vertices.push_back(glm::vec3(x + cell_size, y, 0));
            vertices.push_back(glm::vec3(x + cell_size, y + cell_size, 0));

            normals.push_back(normal);
            normals.push_back(normal);
            normals.push_back(normal);

            add_texc(texcoords, 6, 1);

        }
    }
}

void add_final_floor(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& texcoords, Point p, Point q, float cell_size, bool roof = false){
    glm::vec3 normal = glm::vec3(0.0, 0.0, 0.0);

    float h;
    if (roof){
        h = cell_size;
    } else{
        h = 0;
    }

    p.x = p.x*cell_size;
    p.y = p.y*cell_size;
    q.x = q.x*cell_size;
    q.y = q.y*cell_size;

    vertices.push_back(glm::vec3(p.x, p.y, h));
    vertices.push_back(glm::vec3(p.x, q.y, h));
    vertices.push_back(glm::vec3(q.x, p.y, h));

    normals.push_back(normal);
    normals.push_back(normal);
    normals.push_back(normal);


    add_texc(texcoords, 0, 0);

    vertices.push_back(glm::vec3(p.x, q.y, h));
    vertices.push_back(glm::vec3(q.x, p.y, h));
    vertices.push_back(glm::vec3(q.x, q.y, h));

    normals.push_back(normal);
    normals.push_back(normal);
    normals.push_back(normal);

    add_texc(texcoords, 0, 0);
}





MeshPtr makeRoof(int size, float cell_size, float x, float y)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    float length = size * cell_size;
    x = x * cell_size;
    y = y * cell_size;
    glm::vec3 normal = glm::vec3(0.0, 0.0, 1.0);

    vertices.push_back(glm::vec3(x, y, cell_size));
    vertices.push_back(glm::vec3(x, y + length, cell_size));
    vertices.push_back(glm::vec3(x + length, y, cell_size));

    normals.push_back(normal);
    normals.push_back(normal);
    normals.push_back(normal);


    add_texc(texcoords, 0, 0);

    vertices.push_back(glm::vec3(x, y + length, cell_size));
    vertices.push_back(glm::vec3(x + length, y, cell_size));
    vertices.push_back(glm::vec3(x + length, y + length, cell_size));

    normals.push_back(normal);
    normals.push_back(normal);
    normals.push_back(normal);


    add_texc(texcoords, 0, 0);

    add_final_floor(vertices, normals, texcoords, Point(1, 10), Point(2, 11), cell_size, true);

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Roof is created with " << vertices.size() << " vertices\n";

    return mesh;
}



std::vector <Point> walls = {
    Point(-4, 0), Point(6, 0),
    Point(6, 10), Point(6, 0),
    Point(6, 10), Point(2, 10),
    Point(1, 10), Point(-4, 10),
    Point(-4, 0), Point(-4, 10),

    Point(-3, 1), Point(2, 1),
    Point(3, 1), Point(4, 1),
    Point(5, 1), Point(6, 1),


    Point(-2, 2), Point(-1, 2),
    Point(0, 2), Point(3, 2),
    Point(4, 2), Point(5, 2),

    Point(-3, 3), Point(-2, 3),
    Point(-1, 3), Point(2, 3),
    Point(3, 3), Point(6, 3),

    Point(-2, 4), Point(-1, 4),
    Point(1, 4), Point(3, 4),
    Point(4, 4), Point(5, 4),

    Point(-3, 5), Point(-2, 5),
    Point(2, 5), Point(4, 5),

    Point(-2, 6), Point(-1, 6),
    Point(0, 6), Point(3, 6),
    Point(4, 6), Point(5, 6),

    Point(-1, 7), Point(0, 7),
    Point(-3, 7), Point(-2, 7),
    Point(3, 7), Point(4, 7),

    Point(-2, 8), Point(-1, 8),
    Point(1, 8), Point(3, 8),
    Point(4, 8), Point(5, 8),

    Point(-3, 9), Point(-2, 9),
    Point(0, 9), Point(1, 9),
    Point(3, 9), Point(4, 9),

    Point(-3, 1), Point(-3, 6),
    Point(-3, 7), Point(-3, 9),

    Point(-2, 6), Point(-2, 7),

    Point(-1, 2), Point(-1, 6),
    Point(-1, 7), Point(-1, 10),

    Point(0, 1), Point(0, 2),
    Point(0, 3), Point(0, 6),
    Point(0, 7), Point(0, 8),

    Point(1, 4), Point(1, 5),
    Point(1, 6), Point(1, 7),
    Point(1, 8), Point(1, 10),

    Point(2, 5), Point(2, 6),
    Point(2, 7), Point(2, 8),

    Point(3, 0), Point(3, 1),
    Point(3, 2), Point(3, 4),
    Point(3, 7), Point(3, 9),

    Point(4, 1), Point(4, 2),
    Point(4, 4), Point(4, 5),
    Point(4, 6), Point(4, 7),

    Point(5, 4), Point(5, 6),
    Point(5, 7), Point(5, 10),

    Point(1, 10), Point(1, 11),
    Point(2, 11), Point(1, 11),
    Point(2, 10), Point(2, 11)
};

MeshPtr makeLabyrinth(int size, float cell_size)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    add_floor(vertices, normals, texcoords, size, cell_size, -4, 0);

    for (int i = 0; i < walls.size(); i += 2){
        if(walls[i].x == walls[i+1].x) {
            // Стена вертикальная
            float x = walls[i].x;
            for (float j = std::min(walls[i].y, walls[i + 1].y); j < std::max(walls[i].y, walls[i + 1].y); j++) {
                add_wall(vertices, normals, texcoords, cell_size, Point(x, j), Point(x, j + 1));
            }
        }else{
            //Стена горизонтальная
            float y = walls[i].y;
            for (float j = std::min(walls[i].x, walls[i + 1].x); j < std::max(walls[i].x, walls[i + 1].x); j++) {
                add_wall(vertices, normals, texcoords, cell_size, Point(j, y), Point(j + 1, y));
            }
        }
    }


    add_final_floor(vertices, normals, texcoords, Point(1, 10), Point(2, 11), cell_size);

//    add_wall(vertices, normals, cell_size, Point(0, 0), Point(0, 4));
//    add_wall(vertices, normals, cell_size, Point(0, 0), Point(4, 0));
//    add_wall(vertices, normals, cell_size, Point(4, 4), Point(0, 4));
//    add_wall(vertices, normals, cell_size, Point(4, 3), Point(4, 0));
//    add_wall(vertices, normals, cell_size, Point(3, 1), Point(3, 4));
//    add_wall(vertices, normals, cell_size, Point(3, 1), Point(1, 1));
//    add_wall(vertices, normals, cell_size, Point(1, 2), Point(1, 1));
//    add_wall(vertices, normals, cell_size, Point(1, 2), Point(2, 2));
//    add_wall(vertices, normals, cell_size, Point(2, 3), Point(2, 2));
//
//    add_wall(vertices, normals, cell_size, Point(5, 4), Point(5, 3));
//    add_wall(vertices, normals, cell_size, Point(4, 3), Point(5, 3));
//    add_wall(vertices, normals, cell_size, Point(5, 4), Point(4, 4));





    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());


    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Labyrinth is created with " << vertices.size() << " vertices\n";

    return mesh;
}



MeshPtr makeSphere(float radius, unsigned int N)
{
    unsigned int M = N / 2;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    for (unsigned int i = 0; i < M; i++)
    {
        float theta = (float)glm::pi<float>() * i / M;
        float theta1 = (float)glm::pi<float>() * (i + 1) / M;

        for (unsigned int j = 0; j < N; j++)
        {
            float phi = 2.0f * (float)glm::pi<float>() * j / N + (float)glm::pi<float>();
            float phi1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N + (float)glm::pi<float>();

            //Первый треугольник, образующий квад
            vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta) * radius, sin(phi1) * sin(theta) * radius, cos(theta) * radius));

            normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta), sin(phi1) * sin(theta), cos(theta)));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)i / M));

            //Второй треугольник, образующий квад
            vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
            vertices.push_back(glm::vec3(cos(phi) * sin(theta1) * radius, sin(phi) * sin(theta1) * radius, cos(theta1) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));

            normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
            normals.push_back(glm::vec3(cos(phi) * sin(theta1), sin(phi) * sin(theta1), cos(theta1)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Sphere is created with " << vertices.size() << " vertices\n";

    return mesh;
}
