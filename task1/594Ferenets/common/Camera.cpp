#include <Camera.hpp>

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>

#include <iostream>
#include <fstream>
#include <vector>

struct Point{
    float x;
    float y;
    float z;

    Point(float x = 0, float y = 0, float z = 0): x(x), y(y), z(z) {}
};

std::vector <Point> walls1 = {
        Point(-4, 0), Point(6, 0),
        Point(6, 10), Point(6, 0),
        Point(6, 10), Point(2, 10),
        Point(1, 10), Point(-4, 10),
        Point(-4, 10), Point(-4, 0),

        Point(-3, 1), Point(2, 1),
        Point(3, 1), Point(4, 1),
        Point(5, 1), Point(6, 1),

        Point(-2, 2), Point(-1, 2),
        Point(0, 2), Point(3, 2),
        Point(4, 2), Point(5, 2),

        Point(-3, 3), Point(-2, 3),
        Point(-1, 3), Point(2, 3),
        Point(3, 3), Point(6, 3),

        Point(-2, 4), Point(-1, 4),
        Point(1, 4), Point(3, 4),
        Point(4, 4), Point(5, 4),

        Point(-3, 5), Point(-2, 5),
        Point(2, 5), Point(4, 5),

        Point(-2, 6), Point(-1, 6),
        Point(0, 6), Point(3, 6),
        Point(4, 6), Point(5, 6),

        Point(-1, 7), Point(0, 7),
        Point(-3, 7), Point(-2, 7),
        Point(3, 7), Point(4, 7),

        Point(-2, 8), Point(-1, 8),
        Point(1, 8), Point(3, 8),
        Point(4, 8), Point(5, 8),

        Point(-3, 9), Point(-2, 9),
        Point(0, 9), Point(1, 9),
        Point(3, 9), Point(4, 9),

        Point(-3, 1), Point(-3, 6),
        Point(-3, 7), Point(-3, 9),

        Point(-2, 6), Point(-2, 7),

        Point(-1, 2), Point(-1, 6),
        Point(-1, 7), Point(-1, 10),

        Point(0, 1), Point(0, 2),
        Point(0, 3), Point(0, 6),
        Point(0, 7), Point(0, 8),

        Point(1, 4), Point(1, 5),
        Point(1, 6), Point(1, 7),
        Point(1, 8), Point(1, 10),

        Point(2, 5), Point(2, 6),
        Point(2, 7), Point(2, 8),

        Point(3, 0), Point(3, 1),
        Point(3, 2), Point(3, 4),
        Point(3, 7), Point(3, 9),

        Point(4, 1), Point(4, 2),
        Point(4, 4), Point(4, 5),
        Point(4, 6), Point(4, 7),

        Point(5, 4), Point(5, 6),
        Point(5, 7), Point(5, 10),

        Point(1, 10), Point(1, 11),
        Point(2, 11), Point(1, 11),
        Point(2, 10), Point(2, 11)
};


void OrbitCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void OrbitCameraMover::showOrientationParametersImgui() {
    ImGui::Text("r = %.2f, phi = %.2f, theta = %2f", _r, _phiAng, _thetaAng);
}

void OrbitCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        _phiAng -= dx * 0.005;
        _thetaAng += dy * 0.005;

        _thetaAng = glm::clamp(_thetaAng, -glm::pi<double>() * 0.49, glm::pi<double>() * 0.49);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void OrbitCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
    _r += _r * yoffset * 0.05;
}

void OrbitCameraMover::update(GLFWwindow* window, double dt)
{
    double speed = 1.0;

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        _phiAng -= speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        _phiAng += speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        _thetaAng += speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        _thetaAng -= speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
    {
        _r += _r * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
    {
        _r -= _r * dt;
    }

    _thetaAng = glm::clamp(_thetaAng, -glm::pi<double>() * 0.49, glm::pi<double>() * 0.49);

    //-----------------------------------------

    //Вычисляем положение виртуальной камеры в мировой системе координат по формуле сферических координат
    glm::vec3 pos = glm::vec3(glm::cos(_phiAng) * glm::cos(_thetaAng), glm::sin(_phiAng) * glm::cos(_thetaAng), glm::sin(_thetaAng) + 0.5f) * (float)_r;

    //Обновляем матрицу вида
    _camera.viewMatrix = glm::lookAt(pos, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f));

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}

//=============================================

MyFreeCameraMover::MyFreeCameraMover() :
CameraMover(),
_pos(0.2f, 0.2f, 1.0f)
{
    //Нам нужно как-нибудь посчитать начальную ориентацию камеры
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

void MyFreeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void MyFreeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (true)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворот вверх/вниз
//        glm::vec3 rightDir = glm::vec3(0.0f, 0.0f, 0.0f) * _rot;
//        _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void MyFreeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}



inline float area (Point a, Point b, Point c) {
    return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
}

inline bool intersect_1 (float a, float b, float c, float d) {
    if (a > b)  std::swap (a, b);
    if (c > d)  std::swap (c, d);
    return std::max(a,c) <= std::min(b,d);
}

bool intersect (Point a, Point b, Point c, Point d) {
    return false;
    return intersect_1 (a.x, b.x, c.x, d.x)
           && intersect_1 (a.y, b.y, c.y, d.y)
           && area(a,b,c) * area(a,b,d) <= 0
           && area(c,d,a) * area(c,d,b) <= 0;
}


glm::vec3 MyFreeCameraMover::get_pos(){
    return _pos;
}

glm::quat MyFreeCameraMover::get_rot(){
    return _rot;
}

void MyFreeCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 5.0f;

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    glm::vec3 _old_pos = _pos;

    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        _pos += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        _pos -= forwDir * speed * static_cast<float>(dt);

    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        _pos -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        _pos += rightDir * speed * static_cast<float>(dt);
    }

//    std::cout << _pos.x << " " << _pos.y << std::endl;


    float cell_size = 1.0f;
    for (int i = 0; i < walls1.size(); i+= 2){
        if (intersect(walls1[i], walls1[i + 1], Point(_pos.x/cell_size, _pos.y/cell_size), Point(_old_pos.x/cell_size, _old_pos.y/cell_size))){
//            std::cout << _pos.x << " " << _pos.y << " " << walls1[i].x << " " << walls1[i].y << std::endl;
//            if (walls1[i].x == walls1[i + 1].x) {
//                if (_old_pos.x > cell_size*walls1[i].x) {
//                    _pos.x = cell_size*walls1[i].x + 0.2f;
//                }else{
//                    _pos.x = cell_size*walls1[i].x - 0.2f;
//                }
//            }
//            else {
//                if (_old_pos.y > cell_size*walls1[i].y){
//                    _pos.y = cell_size*walls1[i].y + 0.2f;
//                }else {
//                    _pos.y = cell_size*walls1[i].y - 0.2f;
//                }
//            }

//            if (_old_pos.x > cell_size*walls1[i].x) {
//                _pos.x = cell_size*walls1[i].x + 0.2f;
//            }else{
//                _pos.x = cell_size*walls1[i].x - 0.2f;
//            }
//            if (_old_pos.y > cell_size*walls1[i].y) {
//                _pos.y = cell_size*walls1[i].y + 0.2f;
//            }else{
//                _pos.y = cell_size*walls1[i].y - 0.2f;
//            }
            _pos = _old_pos;
            break;
        }

        if (_pos.x > 1.2 and _pos.x < 1.8 and _pos.y > 10.2 and _pos.y < 10.8){
            std::cout << "YOU WIN" << std::endl;
            exit(0);
        }

    }


    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}



FreeCameraMover::FreeCameraMover() :
        CameraMover(),
        _pos(5.0f, 3.0f, 2.5f)
{
    //Нам нужно как-нибудь посчитать начальную ориентацию камеры
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(1.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

void FreeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void FreeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворот вверх/вниз
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void FreeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void FreeCameraMover::update(GLFWwindow* window, double dt) {
    float speed = 5.0f;

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        _pos += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        _pos -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        _pos -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        _pos += rightDir * speed * static_cast<float>(dt);
    }

    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float) width / height, _near, _far);
}
