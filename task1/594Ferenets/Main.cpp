#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>


#include <iostream>
#include <vector>

bool SEE = true;
bool MY_CAMERA = true;

class SampleApplication : public Application
{
public:
	MeshPtr _roof;
	MeshPtr _labyrinth;

    CameraInfo _mycamera;
    MyFreeCameraMoverPtr _mycameraMover;

    LightInfo _light;
    LightInfo _flashlight;
    MeshPtr _marker;

    float _lr = 5.0;
    float _phi = 90.0;
    float _theta = glm::pi<float>() * 0.25f;

    ShaderProgramPtr _tshader;
    ShaderProgramPtr _markerShader;


    TexturePtr _texture;

    GLuint _sampler;


    void makeScene() override
	{
		Application::makeScene();

		_cameraMover = std::make_shared<FreeCameraMover>();
        _mycameraMover = std::make_shared<MyFreeCameraMover>();

		int size = 10;
		float cell_size = 1.0f;

        _labyrinth = makeLabyrinth(size, cell_size);
        _labyrinth->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

		_roof = makeRoof(size, cell_size, -4, 0);
		_roof->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _marker = makeSphere(0.2f);

        _markerShader = std::make_shared<ShaderProgram>("594FerenetsData1/marker.vert", "594FerenetsData1/marker.frag");
        _tshader = std::make_shared<ShaderProgram>("594FerenetsData1/texture.vert", "594FerenetsData1/texture.frag");

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);


        glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _mycameraMover->get_rot();

        _flashlight.position = _mycameraMover->_pos;
        _flashlight.position.x += forwDir.x;
        _flashlight.position.y += forwDir.y;
        _flashlight.position.z += forwDir.z;
        _flashlight.ambient = glm::vec3(0.2, 0.2, 0.2);
        _flashlight.diffuse = glm::vec3(1.8, 1.8, 1.8);
        _flashlight.specular = glm::vec3(1.0, 1.0, 1.0);

        _texture = loadTexture("594FerenetsData1/images/walls.png");

        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

    void updateGUI() override
    {
        Application::updateGUI();
        if (!MY_CAMERA) {
            ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
            if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
                ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

                if (ImGui::CollapsingHeader("Light")) {
                    ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                    ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                    ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
                }
            }
            ImGui::End();
        }
    }


	void handleKey(int key, int scancode, int action, int mods) override
	{
		Application::handleKey(key, scancode, action, mods);

        _mycameraMover->handleKey(_window, key, scancode, action, mods);
		if (action == GLFW_PRESS)
		{
			if (key == GLFW_KEY_1)
			{
				SEE = false;
			}
			else if (key == GLFW_KEY_2)
			{
				SEE = true;
			}
			else if (key == GLFW_KEY_0)
            {
                MY_CAMERA = true;
            }
            else if (key == GLFW_KEY_9)
            {
                MY_CAMERA = false;
            }
		}

	}

	void handleMouseMove(double xpos, double ypos) override
	{
        glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		Application::handleMouseMove(xpos, ypos);

		_mycameraMover->handleMouseMove(_window, xpos, ypos);
	}


	void update() override{

        double dt = glfwGetTime() - _oldTime;
        _oldTime = glfwGetTime();

        //-----------------------------------------
        if (MY_CAMERA) {
            _mycameraMover->update(_window, dt);
            _mycamera = _mycameraMover->cameraInfo();
        }else{
            _cameraMover->update(_window, dt);
            _camera = _cameraMover->cameraInfo();
        }
	}

	void draw() override
	{
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Устанавливаем шейдер.
		_tshader->use();

		//Устанавливаем общие юниформ-переменные
		if (MY_CAMERA){
            _tshader->setMat4Uniform("viewMatrix", _mycamera.viewMatrix);
            _tshader->setMat4Uniform("projectionMatrix", _mycamera.projMatrix);
		}else{
            _tshader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
            _tshader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
		}
        glm::vec3 lightPosCamSpace;
        glm::vec3 flashlightPosCamSpace;

        if (MY_CAMERA) {
            _flashlight.position = _mycameraMover->get_pos();

            glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _mycameraMover->get_rot();
            _flashlight.position.x += forwDir.x*0.0;
            _flashlight.position.y += forwDir.y*0.0;
            _flashlight.position.z += forwDir.z;
            flashlightPosCamSpace = glm::vec3(_mycamera.viewMatrix * glm::vec4(_flashlight.position, 1.0));

            _tshader->setVec3Uniform("light.pos", flashlightPosCamSpace); //копируем положение уже в системе виртуальной камеры
            _tshader->setVec3Uniform("light.La", _flashlight.ambient);
            _tshader->setVec3Uniform("light.Ld", _flashlight.diffuse);
            _tshader->setVec3Uniform("light.Ls", _flashlight.specular);
        }else{
            _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) *_lr;
            lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
            _tshader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
            _tshader->setVec3Uniform("light.La", _light.ambient);
            _tshader->setVec3Uniform("light.Ld", _light.diffuse);
            _tshader->setVec3Uniform("light.Ls", _light.specular);
        }



        GLuint textureUnitForDiffuseTex = 0;

        if (USE_DSA) {
            glBindTextureUnit(textureUnitForDiffuseTex, _texture->texture());
            glBindSampler(textureUnitForDiffuseTex, _sampler);
        }
        else {
            glBindSampler(textureUnitForDiffuseTex, _sampler);
            glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);  //текстурный юнит 0
            _texture->bind();
        }
        _tshader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);


//        _compass->draw();
        glm::mat4 _viewMatrix;

        if( MY_CAMERA){
            _viewMatrix = _mycamera.viewMatrix;
        }else{
            _viewMatrix = _camera.viewMatrix;
        }

        if(SEE) {
			_tshader->setMat4Uniform("modelMatrix", _roof->modelMatrix());
			_tshader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_viewMatrix * _roof->modelMatrix()))));

			_roof->draw();
		}


        _tshader->setMat4Uniform("modelMatrix", _labyrinth->modelMatrix());
        _tshader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_viewMatrix * _labyrinth->modelMatrix()))));
        _labyrinth->draw();


        {
            if (MY_CAMERA){
                _markerShader->use();

                _markerShader->setMat4Uniform("mvpMatrix", _mycamera.projMatrix * _mycamera.viewMatrix * glm::translate(glm::mat4(1.0f), _flashlight.position));
                _markerShader->setVec4Uniform("color", glm::vec4(_flashlight.diffuse, 1.0f));
//                _marker->draw();
            }
            else{
                _markerShader->use();

                _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
                _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
                _marker->draw();
            }


        }


        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
	}
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}
